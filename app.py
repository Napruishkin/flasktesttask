import json
import openai
from flask import Flask, render_template, request
from typing import List, Dict

from config import FAQ_JSON_FILE, OPENAI_API_KEY

app = Flask(__name__)

# Load FAQ data from the JSON file
with open(FAQ_JSON_FILE, 'r') as file:
    faq_data: List[Dict] = json.load(file)


# Initialize OpenAI API key and ChatGPT context
def initialize_openai():
    openai.api_key = OPENAI_API_KEY

    # Prepare a short message introducing the assistant
    intro_message = {"role": "system", "content": "You are a knowledgeable assistant."}

    # Initialize the model with just the introduction message
    openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[intro_message]
    )


def generate_chatgpt_responses(user_question: str, faq_data: List[Dict]):
    # Find relevant FAQ entries based on keywords in the user's question
    relevant_entries = []
    for entry in faq_data:
        for keyword in entry['Keywords']:
            if keyword.lower() in user_question.lower():
                relevant_entries.append(entry)

    # Create a list of messages to send to ChatGPT
    messages = [{"role": "user", "content": user_question}]
    for entry in relevant_entries:
        messages.append(
            {"role": "assistant", "content": f"Q: {entry['Question_short']}\nA: {entry['Answer_plain_text']}"})

    try:
        # Send the messages to ChatGPT
        response = openai.ChatCompletion.create(
            model="gpt-3.5-turbo",
            messages=messages
        )

        # Extract and return the assistant's response
        if response and response.choices and response.choices[0].message:
            return response.choices[0].message['content']
        else:
            return "ChatGPT response is empty."

    except Exception as e:
        return f"An error occurred: {str(e)}"


# Define the main route for the web application
@app.route('/', methods=['GET', 'POST'])
def index():
    question = ''
    answer = ''

    if request.method == 'POST':
        question = request.form['question']
        chatgpt_answer = generate_chatgpt_responses(question, faq_data)
        answer = chatgpt_answer

    return render_template('index.html', question=question, answer=answer)


# Run the Flask application
if __name__ == '__main__':
    initialize_openai()
    app.run(debug=True)
