# ChatGPT FAQ Assistant

ChatGPT FAQ Assistant is a web application that provides answers to user questions by leveraging the power of ChatGPT, a language model developed by OpenAI. It uses a pre-existing FAQ (Frequently Asked Questions) dataset to enhance the quality and relevance of responses.

## Features

- Responds to user questions based on a dataset of FAQs.
- Utilizes the ChatGPT language model to provide informative answers.
- Allows users to interact with the system via a web interface.

## Prerequisites

Before getting started, make sure you have the following:

- Python 3.x installed
- Flask installed (you can install it using `pip install Flask`)
- An OpenAI API key (you can obtain one from [OpenAI](https://beta.openai.com/signup/))

## Installation

1. Clone this repository to your local machine:

```bash
git clone https://gitlab.com/Napruishkin/flasktesttask.git
```

Navigate to the project directory:
```bash
cd flasktesttask
```

Create a Python virtual environment:
```bash
python -m venv venv
```

Activate the virtual environment:

On macOS and Linux:
```bash
source venv/bin/activate
```

On Windows:
```bash
venv\Scripts\activate
```

Install the required Python packages:
```bash
pip install -r requirements.txt
```

Configure your OpenAI API key:

   Open the `config.py` file and set the `OPENAI_API_KEY` variable to your OpenAI API key.

## Usage

1. Run the application:
```bash
python app.py
```

2. Access the web interface by opening your web browser and navigating to [http://localhost:5000/](http://localhost:5000/).

3. Ask a question in the input field and click "Submit."

4. The application will respond with an answer based on the provided question, utilizing both the FAQ dataset and ChatGPT.

## Customization

You can customize the application by:

- Updating the FAQ dataset in the `FAQ.json` file with your own data.
- Modifying the behavior and responses of the ChatGPT model in the `generate_chatgpt_responses` function in `app.py`.
